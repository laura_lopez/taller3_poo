import random
import pandas as pd


def Lista(a):
    lista=[0]*a
    for i in range(a): 
        lista[i]=random.randint(-100, 100)
    return lista

a=100
aleatorios=Lista(a)
print('Los numeros aleatorios son:')
print(aleatorios)

df = pd.DataFrame(aleatorios)

print('Acontinuacion mostraremos la mediana:')
mediana=df.median()
print(mediana)

print('Acontinuacion mostraremos la media:')
media=df.mean()
print(media)